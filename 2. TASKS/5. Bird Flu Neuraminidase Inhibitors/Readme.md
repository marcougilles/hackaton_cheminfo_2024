## Task: Bird Flu Neuraminidase Inhibitors

## GROUP 5: Bird Flu
- Juarez Marckwordt
- Milova
- Boldtumur
- van Schendel
- **Main coordinator:** Crtomir

## Databases Access
- **ChEMBL**
- **PubChem**
- **BindingDB**
- **IUPHAR**
- **SureChem**

## Software
- **Chemaxon**
- **KNIME**
- **Python (Rdkit, Indigo)**
- **YASARA**

## Tasks

1. **Find the Receptors and Ligands for NeuAc**
   - Search the specified databases to identify and retrieve the receptors and ligands for neuraminidase (NeuAc).

2. **Binding Site Detection**
   - Use the retrieved receptor structures to detect binding sites. This can be done using software tools like YASARA or Chemaxon.

3. **Classify Activities**
   - Classify the interactions based on their nature:
     - Covalent vs. non-covalent binding
     - Allosteric interactions
     - Measurement of binding affinity using parameters like Ki, IC50, and activity levels.

4. **Validation of Docking Experiment**
   - **Redocking with Known Compounds**: Use known neuraminidase inhibitors to validate the docking process. This involves redocking these compounds to ensure the docking protocol is accurate.

5. **Input Data Preparation**
   - **Prepare the Receptors**: Process the receptor structures by cleaning, adding missing atoms, optimizing hydrogen bonding networks, and ensuring the correct protonation states.
   - **Prepare Ligands Database**: Compile a database of potential inhibitors, ensuring the structures are accurate and in the correct format for docking studies.

6. **Perform Virtual Screening**
   - Use virtual screening tools to dock the ligands into the receptor binding sites. This involves setting up and running the docking simulations using software like KNIME or the docking tools available in Chemaxon and YASARA.

7. **Docking**
   - Conduct the docking simulations, ensuring that parameters are optimized for the most accurate results. Document the docking protocol and settings used.

8. **Analyze the Results**
   - Evaluate the docking results by analyzing the binding affinities, binding poses, and interaction patterns. Compare the predicted results with known data to assess the validity and reliability of the docking experiments.

## Repository Management
All materials, including the datasets, scripts, tutorials, and results generated during this task, will be uploaded to a dedicated GitLab repository. This repository will facilitate collaboration and ensure that all data and findings are accessible for future reference and use by the scientific community.