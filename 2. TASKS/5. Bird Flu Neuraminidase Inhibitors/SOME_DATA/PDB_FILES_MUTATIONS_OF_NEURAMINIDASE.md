### SUGESTED PDB FILES FOR STUDY OF MUTATIONS 
(Generated with ChatGPT 4.o) 

Here are some recommended PDB structures of mutated neuraminidase that you can explore for detailed structural information:

1. **H274Y Mutation (N1 subtype)**
   - **PDB ID: 3CKZ**
     - This structure represents the influenza A virus neuraminidase (N1 subtype) with the H274Y mutation, which confers resistance to oseltamivir.

2. **R292K Mutation (N2 subtype)**
   - **PDB ID: 4MWJ**
     - This structure provides insight into the N2 subtype neuraminidase with the R292K mutation, associated with oseltamivir resistance.

3. **E119V Mutation (N2 subtype)**
   - **PDB ID: 4GZX**
     - This PDB entry shows the N2 subtype neuraminidase with the E119V mutation, highlighting changes associated with reduced drug susceptibility.

4. **N294S Mutation (N1 subtype)**
   - **PDB ID: 3NSS**
     - This structure illustrates the N1 subtype neuraminidase with the N294S mutation, linked to reduced sensitivity to oseltamivir.

5. **I222V Mutation (N1 subtype)**
   - **PDB ID: 6Q23**
     - This PDB entry details the neuraminidase from the N1 subtype with the I222V mutation, which is known to affect drug resistance profiles.

6. **G147R Mutation (N2 subtype)**
   - **PDB ID: 3TIA**
     - This structure showcases the N2 subtype neuraminidase with the G147R mutation, providing insights into the mutation's impact on inhibitor binding.

7. **D151G Mutation (Various subtypes)**
   - **PDB ID: 3N8F**
     - This entry includes neuraminidase with the D151G mutation, often associated with reduced susceptibility to neuraminidase inhibitors.

These PDB structures can be accessed and visualized using molecular visualization tools such as PyMOL, Chimera, or online platforms like the RCSB PDB website. They provide detailed structural information that can help in understanding the effects of these mutations on neuraminidase function and drug resistance.